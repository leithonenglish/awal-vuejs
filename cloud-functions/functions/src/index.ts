import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp();

const db = admin.firestore();

export const onDepartmentDelete = functions.firestore
  .document("companies/{comapanyId}/departments/{departmentId}")
  .onDelete(async (snap, context) => {
    const pieces = snap.ref.path.split("/");
    const querySnapshot = await db
      .collection("companies")
      .doc(pieces[1])
      .collection("departments")
      .where("parent", "==", snap.id)
      .get();
    
    if (querySnapshot) {
      querySnapshot.forEach(async doc => {
        await doc.ref.set(
          { parent: admin.firestore.FieldValue.delete() },
          { merge: true }
        );
      });
    }
    return null;
  });
