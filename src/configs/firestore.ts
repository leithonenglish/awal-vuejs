import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyD_GWmZKHTO5_BYp4uukJNx4ven8FMqAE4",
  authDomain: "awal-6062f.firebaseapp.com",
  databaseURL: "https://awal-6062f.firebaseio.com",
  projectId: "awal-6062f",
  storageBucket: "awal-6062f.appspot.com",
  messagingSenderId: "283541560056",
  appId: "1:283541560056:web:40f625e44dcc36163e390d"
});

// Get a Firestore instance
export const db = firebaseApp.firestore();
export const firebaseAuth = firebaseApp.auth();
export const GoogleAuthProvider = firebase.auth.GoogleAuthProvider;

const { Timestamp, FieldPath, FieldValue } = firebase.firestore;
export { Timestamp, FieldPath, FieldValue };
