import Vue from "vue";
import Vuex from "vuex";
import { vuexfireMutations, firestoreAction } from "vuexfire";
import auth from "./modules/auth";
import { db } from "@/configs/firestore";
import RootState from "@/models/interfaces/root-state.iterface";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    pageTitle: "",
    companyId: localStorage.getItem("companyId") || "",
    companies: [],
    departments: []
  },
  mutations: {
    setCompanyId(state: RootState, companyId: string) {
      state.companyId = companyId;
      localStorage.setItem("companyId", companyId);
    },
    setPageTitle(state, title) {
      state.pageTitle = title;
    },
    ...vuexfireMutations
  },
  actions: {
    bindCompanies: firestoreAction(({ bindFirestoreRef }) => {
      return bindFirestoreRef("companies", db.collection("companies"), {
        wait: true
      });
    }),
    bindDepartments: firestoreAction(({ bindFirestoreRef, state }) => {
      const companyId = (state as RootState).companyId;
      return bindFirestoreRef(
        "departments",
        db
          .collection("companies")
          .doc(companyId)
          .collection("departments"),
        { wait: true }
      );
    })
  },
  modules: {
    auth
  }
});
