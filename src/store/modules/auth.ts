import router from "@/router";
import { ActionContext } from "vuex";
import { firebaseAuth, GoogleAuthProvider } from "@/configs/firestore";
import AuthState from "@/models/interfaces/auth-state.interface";
import RootState from "@/models/interfaces/root-state.iterface";

const state = {
  token: localStorage.getItem("token"),
  firstName: localStorage.getItem("firstName"),
  lastName: localStorage.getItem("lastName"),
  imageUrl: localStorage.getItem("imageUrl")
};

const getters = {
  imageUrl: (state: AuthState) =>
    state.imageUrl ? state.imageUrl : require("@/assets/images/person.png"),
  isAuthenticated: (state: AuthState) => !!state.token
};

const actions = {
  login: ({ commit }: ActionContext<AuthState, RootState>, data: any) => {
    const { token, firstName, lastName, roles } = data;
    localStorage.setItem("token", token);
    localStorage.setItem("firstName", firstName);
    localStorage.setItem("lastName", lastName);
    localStorage.setItem("roles", roles);
    commit("setAuthData", data);
  },
  async loginWithGoogleAuthProvider({
    commit
  }: ActionContext<AuthState, RootState>) {
    const provider = new GoogleAuthProvider();
    const response = await firebaseAuth.signInWithPopup(provider);
    if (response.user) {
      const { credential, additionalUserInfo } = response;
      let firstName = null;
      let lastName = null;
      let imageUrl = null;
      if (additionalUserInfo && additionalUserInfo.profile) {
        const profile: any = additionalUserInfo.profile;
        firstName = profile.given_name;
        lastName = profile.family_name;
        imageUrl = profile.picture;
      }

      const token =
        credential && (credential as any).accessToken
          ? (credential as any).accessToken
          : null;

      localStorage.setItem("token", token);
      localStorage.setItem("firstName", firstName);
      localStorage.setItem("lastName", lastName);
      localStorage.setItem("imageUrl", imageUrl);

      commit("setAuthData", {
        firstName,
        lastName,
        imageUrl,
        token
      });
      router.push({ name: "timeOff" });
    }
  },
  logout: async ({ commit }: ActionContext<AuthState, RootState>) => {
    router.push({ name: "Login" });
    await firebaseAuth.signOut();
    localStorage.clear();
    commit("removeAuthData");
  }
};

const mutations = {
  setAuthData(state: AuthState, data: any) {
    const { token, firstName, lastName, imageUrl } = data;
    state.token = token;
    state.firstName = firstName;
    state.lastName = lastName;
    //state.roles = roles;
    state.imageUrl = imageUrl;
  },
  removeAuthData(state: AuthState) {
    state.token = null;
    state.firstName = null;
    state.lastName = null;
    state.roles = null;
    state.imageUrl = null;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
