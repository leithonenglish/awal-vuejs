import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import store from "@/store";
import Main from "../views/Main.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    component: Main,
    beforeEnter: async (_to, _from, next) => {
      const isAuthenticated = store.getters["auth/isAuthenticated"];
      if (!isAuthenticated) {
        next({ name: "Login" });
      } else {
        const companies = store.state.companies;
        if (!companies || (companies && companies.length < 1)) {
          await store.dispatch("bindCompanies");
          if (
            !store.state.companyId &&
            store.state.companies &&
            store.state.companies.length > 0
          ) {
            store.commit("setCompanyId", store.state.companies[0].id);
            await store.dispatch("bindDepartments");
          }
        }
        next();
      }
    },
    children: [
      {
        path: "/",
        name: "timeOff",
        meta: { title: "Time Off" },
        component: () =>
          import(/* webpackChunkName: "home" */ "../views/Home.vue")
      },
      {
        path: "/people",
        name: "people",
        meta: { title: "People" },
        component: () =>
          import(/* webpackChunkName: "people" */ "../views/People.vue")
      }
    ]
  },
  {
    path: "/auth",
    component: () => import("../views/auth/Auth.vue"),
    redirect: { name: "Login" },
    children: [
      {
        path: "login",
        name: "Login",
        meta: {
          title: "Login"
        },
        component: () => import("../views/auth/Login.vue")
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  let title = to.meta.title;
  if (!title && to.matched.length > 2) {
    title = to.matched[1].meta.title;
  }
  if (title) {
    document.title = `AWAL | ${title}`;
    store.commit("setPageTitle", title);
  } else {
    document.title = "AWAL";
  }
  next();
});

export default router;
