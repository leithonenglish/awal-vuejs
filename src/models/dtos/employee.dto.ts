import Address from "../address";
import Email from "../email";
import LeaveAllotment from "../leave-allotment";
import Phone from "../phone";
import { Status, Gender } from "../enums/employee";

export default class EmployeeDTO {
  id: string | undefined;
  position = "";
  firstName = "";
  middleInitials: string | undefined;
  lastName = "";
  gender = Gender.UNSPECIFIED;
  married = false;
  birthday = new Date();
  address = new Address();
  phones: Phone[] = [];
  emails: Email[] = [];
  extension: number | undefined;
  dateHired = new Date();
  dateLeft: Date | undefined;
  worksSundays = false;
  worksSaturdays = false;
  worksHolidays = false;
  worksShifts = false;
  status = Status.INACTIVE;
  leaveAllotments: LeaveAllotment[] | undefined;
}
