export default class DepartmentDTO {
  id: string | undefined;
  name = "";
  parent: string | undefined;
}
