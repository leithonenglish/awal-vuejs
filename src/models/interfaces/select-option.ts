export default interface SelectOption {
  label: string
  value: any
  children?: SelectOption[]
  attrs?: Record<string, any>
}
