import Division from "./division.interface";

export default interface DivisionByCountry {
  [key: string]: Division[]
}
