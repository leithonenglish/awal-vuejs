export default interface ICompany {
  id: string;
  name: string;
}
