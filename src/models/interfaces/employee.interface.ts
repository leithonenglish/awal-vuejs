import IPosition from "./position.interface";
import Address from "../address";
import Email from "../email";
import LeaveAllotment from "../leave-allotment";
import Phone from "../phone";
import { Status, Gender } from '../enums/employee';

export default interface IEmployee
{
  id: string | undefined;
  position: IPosition;
  firstName: string;
  middleInitials: string | undefined;
  lastName: string;
  gender: Gender;
  married: boolean;
  birthday: Date;
  address: Address;
  phones: Phone[];
  emails: Email[];
  extension: number | null;
  dateHired: Date;
  dateLeft: Date | undefined;
  worksSundays: boolean;
  worksSaturdays: boolean;
  worksHolidays: boolean;
  worksShifts: boolean;
  status: Status;
  leaveAllotments: LeaveAllotment[] | undefined;
}
