export default interface AuthState {
  token: string | null,
  firstName: string | null,
  lastName: string | null,
  imageUrl: string | null,
  roles: string[] | null
}
