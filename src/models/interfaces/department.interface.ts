export default interface IDepartment {
  id: string;
  name: string;
  parent: string | undefined;
}
