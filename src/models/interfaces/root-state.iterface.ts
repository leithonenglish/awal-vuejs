import ICompany from "./company.interface";
import IDepartment from "./department.interface";

export default interface RootState {
  pageTitle: string,
  companyId: string,
  companies: ICompany[],
  departments: IDepartment[]
}
