export default interface IPosition {
  id: string;
  name: string;
}
