export default interface Division {
  name: string;
  abbreviation: string;
}
