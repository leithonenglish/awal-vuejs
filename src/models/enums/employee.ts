export enum Status {
  INACTIVE = 1,
  ACTIVE,
  SUSPENDED,
  PROBATION
}

export enum Gender {
  MALE = 1,
  FEMALE,
  UNSPECIFIED
}

export enum PhoneType {
  WORK = 1,
  CELL,
  HOME
}
