export default class Address {
  line1 = "";
  line2: string | null = null;
  line3: string | null = null;
  city = "";
  division = "";
  country = "";
}
