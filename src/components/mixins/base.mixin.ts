import { Component, Vue, Watch } from "vue-property-decorator";
import * as _ from "lodash";

@Component
export default class BaseMixin<T> extends Vue {
  list: T[] | null = null;
  loaded = false;

  get companyId(): string {
    return this.$store.state.companyId;
  }

  get hasData() {
    return this.list && this.list.length > 0;
  }

  beforeMount() {
    this.setupData(true);
  }

  destroyed() {
    this.$unbind("list");
  }

  @Watch("companyId")
  onCompanyIdChanged() {
    this.setupData();
  }

  @Watch("list")
  onListChanged() {
    if (this.list !== null) {
      this.loaded = true;
    }
  }

  setupData(_ = false): void {
    return;
  }

  properCase(str: string): string {
    return _.startCase(_.toLower(str));
  }
}
