import { Component, Prop, Vue, Watch } from "vue-property-decorator";
import * as _ from "lodash";

@Component
export default class DialogFormMixin<T> extends Vue {
  @Prop({ type: Object })
  value!: T;
  @Prop({ type: Boolean, required: true })
  visible = false;
  dialogOpen = false;
  dto!: T;

  @Watch("value")
  onValueChange() {
    if (this.value) {
      this.dto = _.clone(this.value);
    }
  }

  @Watch("visible")
  onVisibleChanged() {
    this.dialogOpen = this.visible;
  }

  @Watch("dialogOpen")
  onDialogOpenChanged() {
    this.$emit("update:visible", this.dialogOpen);
  }
}
