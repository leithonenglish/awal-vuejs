import SelectOption from "@/models/interfaces/select-option";
import DivisionByCountry from "@/models/interfaces/division-by-country.interface";
import countries from "@/assets/data/countries.json";
import divisions from "@/assets/data/divisions.json";

class AddressService {
  getCountries(): SelectOption[] {
    return countries.map(country => {
      return { label: country.name, value: country.code };
    });
  }

  getDivisions(country: string): SelectOption[] {
    if (country in divisions) {
      const divisionsByCountry = (divisions as DivisionByCountry)[country];
      return divisionsByCountry.map((division) => {
        return { label: division.name, value: division.abbreviation };
      });
    }
    return [];
  }

  getDivisionLabel(country: string | null) {
    if (country) {
      switch (country) {
        case "US":
          return "State";
        case "CA":
          return "Province";
        case "JM":
          return "Parish";
        default:
          return "Divisions";
      }
    }
    return "Parish/State/Province";
  }

  isDivision(country: string) {
    return ["US", "CA", "JM"].includes(country);
  }
}

const addressService = new AddressService();

export default addressService;
