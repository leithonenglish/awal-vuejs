import store from "@/store";
import DepartmentDTO from "@/models/dtos/department.dto";
import { db } from "@/configs/firestore";

class DepartmentService {
  async addOrUpdate(dto: DepartmentDTO) {
    const isUpdate = !!dto.id;
    const collectionRef = db
      .collection("companies")
      .doc(store.state.companyId)
      .collection("departments");

    const docRef = isUpdate ? collectionRef.doc(dto.id) : collectionRef.doc();

    await docRef.set({ ...dto }, { merge: isUpdate });
  }

  async delete(id: string) {
    await db
      .collection("companies")
      .doc(store.state.companyId)
      .collection("departments")
      .doc(id)
      .delete();
  }
}
const departmentService = new DepartmentService();
export default departmentService;
