import store from "@/store";
import EmployeeDTO from "@/models/dtos/employee.dto";
import { db } from "@/configs/firestore";

class EmployeeService {
  async addOrUpdate(dto: EmployeeDTO) {
    const isUpdate = !!dto.id;
    const collectionRef = db
      .collection("companies")
      .doc(store.state.companyId)
      .collection("employees");

    const docRef = isUpdate ? collectionRef.doc(dto.id) : collectionRef.doc();
    await docRef.set({ ...dto }, { merge: isUpdate });
  }

  async delete(id: string) {
    await db
      .collection("companies")
      .doc(store.state.companyId)
      .collection("employees")
      .doc(id)
      .delete();
  }
}
const employeeService = new EmployeeService();
export default employeeService;
